# Projeto Teste Unitário e Integração

Esse projeto é um tem como objetivo praticar teste unitário e integração.

O objetivo foi criar uma API Rest com dois CRUD usando a abordagem do TDD.

Foram criado um ambiente do Jenkins para deploy e SonarQube para analisar a qualidade do codígo e medir percentual de cobertura de teste.
 

# O desafio foi usado as seguintes tecnologias

  - **Swagger** (Usado para documentar API e os end-points) 
  - **Docker** (Usando para executar o ambiente de banco e sonar)
  - **Java 11**
  - **Junit** (Para os testes unitários e integrados)
  - **Mockito** (Para mockar os dados do teste unitário)
  - **Spring Boot 2.5.3 - Maven**
  - **Lombok** (Lombok é uma biblioteca java com foco em produtividade e redução de código)
  - **Postgres** (Banco de dados)
  - **NGINX** (Servidor de proxy reverso)


# Teste

Teste a aplicação no Swagger ou fazendo a requisição do end-point no browser.

Swagger: http://localhost:8080/app/swagger-ui/index.html?configUrl=/app/v3/api-docs/swagger-config

![header][image-swagger]


Jenkins

![header][image-jenkins]


SonarQube

![header][image-sonar]





[image-swagger]: https://gitlab.com/rodolfo.rodrigues374/teste-unitario-app/-/raw/master/img/swagger.png
[image-jenkins]: https://gitlab.com/rodolfo.rodrigues374/teste-unitario-app/-/raw/master/img/jenkins.png
[image-sonar]: https://gitlab.com/rodolfo.rodrigues374/teste-unitario-app/-/raw/master/img/sonar.png
