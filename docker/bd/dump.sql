SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


CREATE DATABASE testeunitarioapp WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.utf8' LC_CTYPE = 'pt_BR.utf8';

  

ALTER DATABASE testeunitarioapp OWNER TO postgresadmin;

\connect testeunitarioapp

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE SCHEMA sch_app AUTHORIZATION postgresadmin;

SELECT schema_name     FROM information_schema.schemata    WHERE schema_name NOT LIKE 'pg_%' AND schema_name != 'information_schema';



CREATE TABLE sch_app.t_usuario (
	id_usuario bigserial NOT NULL,	 
	nome varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	data_nascimento timestamp NOT NULL,
	data_criacao timestamp NOT NULL DEFAULT now(),
	data_alteracao timestamp NOT NULL DEFAULT now(),		
	CONSTRAINT pk_produto PRIMARY KEY (id_usuario)
);


CREATE SEQUENCE sch_app.t_usuario_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 69
	CACHE 1
	NO CYCLE;

	
INSERT INTO sch_app.t_usuario	(nome, email, data_nascimento, data_criacao, data_alteracao) VALUES ('Rodolfo', 'rodolfo@com.br', '1989-08-23 22:32:01.634275', now(), now());
INSERT INTO sch_app.t_usuario	(nome, email, data_nascimento, data_criacao, data_alteracao) VALUES ('Manolo', 'manolo@com.br', '1989-08-23 22:32:01.634275', now(), now());

select * from sch_app.t_usuario;

CREATE TABLE sch_app.t_conta (
	id_conta bigserial NOT NULL,
	id_usuario bigserial NOT NULL,
	numero_conta varchar(30) NOT NULL,
	numero_agencia varchar(30) NOT NULL,
	tipo_conta int NOT NULL,
	data_criacao timestamp NOT NULL DEFAULT now(),
	data_alteracao timestamp NOT NULL DEFAULT now(),		
	CONSTRAINT pk_conta PRIMARY KEY (id_conta),
	CONSTRAINT fk_conta_x_usuario FOREIGN KEY (id_usuario) REFERENCES  sch_app.t_usuario(id_usuario)
);


CREATE SEQUENCE sch_app.t_conta_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 69
	CACHE 1
	NO CYCLE;

 
INSERT INTO sch_app.t_conta (numero_conta,numero_agencia,tipo_conta,data_criacao,data_alteracao) VALUES	 ('1234','12345',1,now(),now());
INSERT INTO sch_app.t_conta (numero_conta,numero_agencia,tipo_conta,data_criacao,data_alteracao) VALUES	 ('8545','98562',1,now(),now());

select * from sch_app.t_conta;


