package com.teste.unitario.app.mapper;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;
import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;
import com.teste.unitario.app.entity.UsuarioEntity;

@Service
public class UsuarioMapper {

	@Autowired
	private ModelMapper modelMapper;

	public UsuarioResponse mapper(UsuarioEntity origem) {
		return this.modelMapper.map(origem, UsuarioResponse.class);
	}

	public List<UsuarioResponse> mapper(List<UsuarioEntity> list) {
		return modelMapper.map(list, new TypeToken<List<UsuarioResponse>>() {
		}.getType());
	}

	public UsuarioEntity mapper(UsuarioCadastrarRequest request) {
		var obj = modelMapper.map(request, UsuarioEntity.class);
		obj.setDataNascimento(LocalDateTime.now());
		obj.setDataCriacao(LocalDateTime.now());
		obj.setDataAlteracao(LocalDateTime.now());
		return obj;
	}

}
