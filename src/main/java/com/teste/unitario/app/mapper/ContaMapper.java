package com.teste.unitario.app.mapper;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.unitario.app.dto.request.conta.ContaCadastrarRequest;
import com.teste.unitario.app.dto.request.conta.ContaEditarRequest;
import com.teste.unitario.app.dto.response.conta.ContaResponse;
import com.teste.unitario.app.entity.ContaEntity;

@Service
public class ContaMapper {

	@Autowired
	private ModelMapper modelMapper;

	public ContaResponse mapper(ContaEntity origem) {
		return this.modelMapper.map(origem, ContaResponse.class);
	}

	public List<ContaResponse> mapper(List<ContaEntity> list) {
		return modelMapper.map(list, new TypeToken<List<ContaResponse>>() {
		}.getType());
	}

	public ContaEntity mapper(ContaCadastrarRequest request) {
		var obj = modelMapper.map(request, ContaEntity.class);		
		obj.setDataCriacao(LocalDateTime.now());
		obj.setDataAlteracao(LocalDateTime.now());
		return obj;
	}
	
	public ContaEntity mapper(ContaEditarRequest request) {
		var obj = modelMapper.map(request, ContaEntity.class);	
		obj.setDataAlteracao(LocalDateTime.now());
		return obj;
	}

}
