package com.teste.unitario.app.dto.request.conta;

import java.io.Serializable;
import java.math.BigInteger;

import javax.validation.constraints.NotNull;

import com.teste.unitario.app.enuns.TipoContaEnum;
import com.teste.unitario.app.utils.Mensagem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContaEditarRequest implements Serializable{
 
	private static final long serialVersionUID = -6762259429927680998L;
	
	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private BigInteger id;

	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private BigInteger idUsuario;
	
	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private String numConta;

	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private String numAgencia;
	
	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private TipoContaEnum tipoConta;
}
