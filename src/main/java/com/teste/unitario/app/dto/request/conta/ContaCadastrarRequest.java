package com.teste.unitario.app.dto.request.conta;

import java.io.Serializable;
import java.math.BigInteger;

import com.teste.unitario.app.enuns.TipoContaEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContaCadastrarRequest implements Serializable{
 
	private static final long serialVersionUID = -6762259429927680998L;

	private BigInteger idUsuario;
	
	private String numConta;

	private String numAgencia;
	
	private TipoContaEnum tipoConta;
}
