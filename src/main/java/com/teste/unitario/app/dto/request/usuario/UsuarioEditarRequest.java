package com.teste.unitario.app.dto.request.usuario;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.teste.unitario.app.utils.Mensagem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioEditarRequest implements Serializable {

	private static final long serialVersionUID = 5190839226445263035L;

	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private BigInteger id;

	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private String nome;

	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	private String email;

	@NotNull(message = Mensagem.CAMPO_OBRIGATORIO)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dataNascimento;
}
