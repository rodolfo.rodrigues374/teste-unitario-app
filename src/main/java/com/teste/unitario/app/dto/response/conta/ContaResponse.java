package com.teste.unitario.app.dto.response.conta;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.teste.unitario.app.enuns.TipoContaEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContaResponse implements Serializable {
	
	private static final long serialVersionUID = -3234380152571660445L;

	private BigInteger idConta;
	
	private BigInteger idUsuario;

	private String numConta;

	private String numAgencia;

	private TipoContaEnum tipoConta;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dataCriacao;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dataAlteracao;

}
