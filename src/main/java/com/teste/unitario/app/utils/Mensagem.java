package com.teste.unitario.app.utils;

public class Mensagem {
	
	private Mensagem() {
		
	}

	public static final String REGISTRO_NAO_ENCONTRADO = "Registro não encontrado!";
	public static final String CODIGO_OBRIGATORIO = "Codígo obrigatório!";
	public static final String CAMPO_OBRIGATORIO = "Campo obrigatório!";
	public static final String EMAIL_CADASTRADO = "E-mail cadastrado!";
	public static final String EMAIL_INVALIDO = "E-mail inválido!";
	public static final String NUM_AGENCIA_CADASTRADO = "Número agencia já cadastrado!";
	public static final String NUM_CONTA_CADASTRADO = "Número conta já cadastrado!";
	public static final String USUARIO_NAO_ENCONTRADO = "Usuário não encontrado!";
	public static final String USUARIO_IDADE_MENOR_18_ANOS = "Usuário menor de 18 anos!";
}
