package com.teste.unitario.app.utils;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ValidationException;

public class Validacao {

	private Validacao() {

	}

	public static void validEmailAddressRegex(String email) {
		boolean isEmailIdValid = false;
		if (email != null && email.length() > 0) {
			String regex = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";// a regular expression
			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(email);
			if (matcher.matches()) {
				isEmailIdValid = true;
			}
		}

		if (!isEmailIdValid) {
			throw new ValidationException(Mensagem.EMAIL_INVALIDO);
		}
	}

	public static void campoObrigatorio(String valor) {

		if (valor == null || valor.isEmpty()) {
			throw new ValidationException(Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	public static void campoObrigatorio(LocalDateTime valor) {

		if (valor == null) {
			throw new ValidationException(Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	public static void campoObrigatorio(Object valor) {

		if (valor == null) {
			throw new ValidationException(Mensagem.CAMPO_OBRIGATORIO);
		}
	}
}
