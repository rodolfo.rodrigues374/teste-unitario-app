package com.teste.unitario.app.repository;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.teste.unitario.app.entity.ContaEntity;

@Repository
public interface ContaRepository extends JpaRepository<ContaEntity, BigInteger> {   

	Optional<ContaEntity> findByNumAgencia(final String numAgencia);
	
	Optional<ContaEntity> findByNumConta(final String numConta);
}
