package com.teste.unitario.app.repository;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.teste.unitario.app.entity.UsuarioEntity;

@Repository
public interface UsuarioRepository  extends JpaRepository<UsuarioEntity, BigInteger> { 

	Optional<UsuarioEntity> findByEmail(final String email);
	
	Optional<UsuarioEntity> findById(final BigInteger id);
}
