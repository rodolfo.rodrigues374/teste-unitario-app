package com.teste.unitario.app.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.teste.unitario.app.enuns.TipoContaEnum;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
@Entity(name = "t_conta")
public class ContaEntity  extends BaseEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tContaSeq")
	@SequenceGenerator(name = "tContaSeq", sequenceName = "t_conta_seq", allocationSize = 1)
	@Column(name = "id_conta")
	private BigInteger idConta;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario", foreignKey = @ForeignKey(name = "fk_conta_x_usuario")  )
	private UsuarioEntity usuario;
	
	@Column(name = "numero_conta")
	private String numConta;
	
	@Column(name = "numero_agencia")
	private String numAgencia;
	
	@NotNull
	@Column(name = "tipo_conta")
	private TipoContaEnum tipoConta;
	
	
	
}
