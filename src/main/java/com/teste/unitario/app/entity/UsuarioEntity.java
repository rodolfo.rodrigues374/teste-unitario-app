package com.teste.unitario.app.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "t_usuario")
public class UsuarioEntity extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tUsuarioSeq")
	@SequenceGenerator(name = "tUsuarioSeq", sequenceName = "t_usuario_seq", allocationSize = 1)
	@Column(name = "id_usuario")
	private BigInteger idUsuario;

	@Column(name = "nome")
	private String nome;

	@Column(name = "email")
	private String email;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "data_nascimento")
	private LocalDateTime dataNascimento;
}
