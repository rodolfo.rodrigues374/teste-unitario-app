package com.teste.unitario.app.service.interfaces;

import java.math.BigInteger;
import java.util.List;

import com.teste.unitario.app.dto.request.conta.ContaCadastrarRequest;
import com.teste.unitario.app.dto.request.conta.ContaEditarRequest;
import com.teste.unitario.app.dto.response.conta.ContaResponse;

public interface IContaService {

	List<ContaResponse> listar() ;
	void cadastrar(ContaCadastrarRequest request);
	void editar(ContaEditarRequest request);
	ContaResponse obter(BigInteger id);
	void deletar(BigInteger id);
}
