package com.teste.unitario.app.service.impl;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;
import com.teste.unitario.app.dto.request.usuario.UsuarioEditarRequest;
import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;
import com.teste.unitario.app.entity.UsuarioEntity;
import com.teste.unitario.app.mapper.UsuarioMapper;
import com.teste.unitario.app.repository.UsuarioRepository;
import com.teste.unitario.app.service.interfaces.IUsuarioService;
import com.teste.unitario.app.utils.Mensagem;
import com.teste.unitario.app.utils.Validacao;

@Service
public class UsuarioService implements IUsuarioService {
	
	@Autowired
	private UsuarioMapper usuarioMapper;

	@Autowired
	private UsuarioRepository usuarioRepository;
  	
	public List<UsuarioResponse> listar() { 
		return this.usuarioMapper.mapper(this.usuarioRepository.findAll());
	}

	public void cadastrar(UsuarioCadastrarRequest request) {
		 
		campoObrigatorios(request);
		var usuario = this.usuarioMapper.mapper(request);

		Validacao.validEmailAddressRegex(usuario.getEmail());

		verificaEmailCadastrado(usuario);

		this.usuarioRepository.save(usuario);

	}

	public void editar(UsuarioEditarRequest request) {

		if (request.getId() == null)
			throw new ValidationException(Mensagem.CODIGO_OBRIGATORIO);

		var usuario = this.usuarioRepository.findById(request.getId());

		if (!usuario.isPresent())
			throw new ValidationException(Mensagem.REGISTRO_NAO_ENCONTRADO);

		var entity = usuario.get();
		entity.setNome(request.getNome());
		entity.setEmail(request.getEmail());
		entity.setDataNascimento(request.getDataNascimento());
		entity.setDataAlteracao(LocalDateTime.now());

		this.usuarioRepository.save(entity);
	}
	 	
	public UsuarioResponse obter(BigInteger id) {
		 
		if (id == null)
			throw new ValidationException(Mensagem.CODIGO_OBRIGATORIO);

		var usuario = this.usuarioRepository.findById(id);

		if (!usuario.isPresent())
			throw new ValidationException(Mensagem.REGISTRO_NAO_ENCONTRADO);

		var entity = usuario.get();

		return this.usuarioMapper.mapper(entity);
		 
	}
 
	public void deletar(BigInteger id) {

		if (id == null)
			throw new ValidationException(Mensagem.CODIGO_OBRIGATORIO);

		var usuario = this.usuarioRepository.findById(id);

		if (!usuario.isPresent())
			throw new ValidationException(Mensagem.REGISTRO_NAO_ENCONTRADO);

		var entity = usuario.get();

		this.usuarioRepository.delete(entity);
	}

	private void campoObrigatorios(UsuarioCadastrarRequest request) {

		Validacao.campoObrigatorio(request.getNome());
		Validacao.campoObrigatorio(request.getEmail());
		Validacao.campoObrigatorio(request.getDataNascimento());
	}

	private void verificaEmailCadastrado(UsuarioEntity usuario) {

		var usuarioEmail = this.usuarioRepository.findByEmail(usuario.getEmail());

		if (usuarioEmail.isPresent()) {
			throw new ValidationException(Mensagem.EMAIL_CADASTRADO);
		}
	} 
}
