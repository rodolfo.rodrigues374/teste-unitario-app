package com.teste.unitario.app.service.impl;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.unitario.app.dto.request.conta.ContaCadastrarRequest;
import com.teste.unitario.app.dto.request.conta.ContaEditarRequest;
import com.teste.unitario.app.dto.response.conta.ContaResponse;
import com.teste.unitario.app.entity.ContaEntity;
import com.teste.unitario.app.mapper.ContaMapper;
import com.teste.unitario.app.repository.ContaRepository;
import com.teste.unitario.app.repository.UsuarioRepository;
import com.teste.unitario.app.service.interfaces.IContaService;
import com.teste.unitario.app.utils.Mensagem;
import com.teste.unitario.app.utils.Validacao;

@Service
public class ContaService  implements IContaService {

	@Autowired
	private ContaMapper contaMapper;
	 
	@Autowired
	private ContaRepository contaRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	public List<ContaResponse> listar() {
		return this.contaMapper.mapper(this.contaRepository.findAll());
	}

	public void cadastrar(ContaCadastrarRequest request) {
  
		campoObrigatorios(request);

		var conta = this.contaMapper.mapper(request);
		validacao(conta);
		this.contaRepository.save(conta);

	}
	
	private void validacao(ContaEntity conta) {
		verificaNumAgenciaCadastrado(conta.getNumAgencia());
		verificaNumContaCadastrado(conta.getNumConta());
		verificaSeUsuarioEhMario18Anos(conta.getUsuario().getIdUsuario());
	}

	private void verificaNumAgenciaCadastrado(String numAgencia) {

		var conta = this.contaRepository.findByNumAgencia(numAgencia);

		if (conta.isPresent()) {
			throw new ValidationException(Mensagem.NUM_AGENCIA_CADASTRADO);
		}
	}

	private void verificaNumContaCadastrado(String numConta) {

		var conta = this.contaRepository.findByNumConta(numConta);

		if (conta.isPresent()) {
			throw new ValidationException(Mensagem.NUM_CONTA_CADASTRADO);
		}
	}

	private void verificaSeUsuarioEhMario18Anos(BigInteger idUsuario) {

		if (idUsuario == null) {
			throw new ValidationException(Mensagem.USUARIO_NAO_ENCONTRADO);
		}

		var usuario = this.usuarioRepository.findById(idUsuario);

		if (usuario.isEmpty()) {
			throw new ValidationException(Mensagem.USUARIO_NAO_ENCONTRADO);
		}

		var totalAnos = LocalDateTime.now().getYear() - usuario.get().getDataNascimento().getYear();
		if (totalAnos < 18) {
			throw new ValidationException(Mensagem.USUARIO_IDADE_MENOR_18_ANOS);
		}
	}

	public void editar(ContaEditarRequest request) {
		
		if (request.getId() == null)
			throw new ValidationException(Mensagem.CODIGO_OBRIGATORIO);

		campoObrigatorios(request);
		
		var conta = this.contaRepository.findById(request.getId());

		if (!conta.isPresent())
			throw new ValidationException(Mensagem.REGISTRO_NAO_ENCONTRADO);
		
		var contaEntity = this.contaMapper.mapper(request);		  
		contaEntity.setIdConta(conta.get().getIdConta());
		
		this.contaRepository.save(contaEntity);
	}
	 	
	public ContaResponse obter(BigInteger id) {
		 
		if (id == null)
			throw new ValidationException(Mensagem.CODIGO_OBRIGATORIO);

		var conta = this.contaRepository.findById(id);

		if (!conta.isPresent())
			throw new ValidationException(Mensagem.REGISTRO_NAO_ENCONTRADO);

		var entity = conta.get();

		return this.contaMapper.mapper(entity);
		 
	}
 
	public void deletar(BigInteger id) {

		if (id == null)
			throw new ValidationException(Mensagem.CODIGO_OBRIGATORIO);

		var conta = this.contaRepository.findById(id);

		if (!conta.isPresent())
			throw new ValidationException(Mensagem.REGISTRO_NAO_ENCONTRADO);

		var entity = conta.get();

		this.contaRepository.delete(entity);
	}

	private void campoObrigatorios(ContaCadastrarRequest request) {

		Validacao.campoObrigatorio(request.getNumAgencia());
		Validacao.campoObrigatorio(request.getNumConta());
		Validacao.campoObrigatorio(request.getTipoConta());
		Validacao.campoObrigatorio(request.getIdUsuario());
	}
	
	private void campoObrigatorios(ContaEditarRequest request) {

		Validacao.campoObrigatorio(request.getNumAgencia());
		Validacao.campoObrigatorio(request.getNumConta());
		Validacao.campoObrigatorio(request.getTipoConta());
		Validacao.campoObrigatorio(request.getIdUsuario());
	}

  
}
