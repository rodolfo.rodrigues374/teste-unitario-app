package com.teste.unitario.app.service.interfaces;

import java.math.BigInteger;
import java.util.List;

import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;
import com.teste.unitario.app.dto.request.usuario.UsuarioEditarRequest;
import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;

public interface IUsuarioService {

	List<UsuarioResponse> listar() ;
	void cadastrar(UsuarioCadastrarRequest request);
	void editar(UsuarioEditarRequest request);
	UsuarioResponse obter(BigInteger id);
	void deletar(BigInteger id);
}
