package com.teste.unitario.app.enuns;

public enum TipoContaEnum {

	CORRENTE(1, "Corrente"),
	POUPANCA(2, "Poupança"), 
	DIGITAL(3, "Digital");

	public static TipoContaEnum valueOf(final int value) {
		for (final TipoContaEnum type : values()) {
			if (type.value == value) {
				return type;
			}
		}
		return TipoContaEnum.DIGITAL;
	}

	private final Integer value;

	private final String text;

	TipoContaEnum(final Integer value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public Integer getValue() {
		return this.value;
	}

	public Integer value() {
		return this.value;
	}
}
