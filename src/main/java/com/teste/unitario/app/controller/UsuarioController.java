package com.teste.unitario.app.controller;

import java.math.BigInteger;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;
import com.teste.unitario.app.dto.request.usuario.UsuarioEditarRequest;
import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;
import com.teste.unitario.app.service.interfaces.IUsuarioService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/usuario")
@Tag(name = "usuario", description = "Usuário API")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class UsuarioController extends BaseController {

	@Autowired
	private IUsuarioService usuarioService;

	@Operation(summary = "Retorna uma lista de usuários", description = "Essa operação retorna as informações de usuários.", tags = {
			"usuário" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Lista de usuários retornada com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UsuarioResponse.class)))) })
	@GetMapping(value = "/get")
	public ResponseEntity get() {

		try {
			var lista = this.usuarioService.listar();
			return new ResponseEntity(lista, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Salva o usuário", description = "Essa operação salva as informações de usuários.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Usuário salvo com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UsuarioCadastrarRequest.class)))) })
	@PostMapping(value = "/save")
	public ResponseEntity save(@RequestBody @Valid UsuarioCadastrarRequest request) {

		try {
			this.usuarioService.cadastrar(request);
			return new ResponseEntity(HttpStatus.CREATED);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Pesquisa o usuário", description = "Essa operação pesquisa as informações do usuário por código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Usuário pesquisado com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UsuarioResponse.class)))) })
	@GetMapping(value = "/get/{id}")
	public ResponseEntity getId(
			@Parameter(name = "id", description = "Codígo do usuário.", example = "123", required = true) @PathVariable BigInteger id) {

		try {

			return new ResponseEntity(this.usuarioService.obter(id), HttpStatus.OK);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Alteração do usuário", description = "Essa operação altera as informações do usuário por código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Usuário alerado com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UsuarioEditarRequest.class)))) })
	@PutMapping(value = "/edit")
	public ResponseEntity edit(@RequestBody @Valid UsuarioEditarRequest request) {

		try {
			this.usuarioService.editar(request);
			return new ResponseEntity(HttpStatus.OK);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Exclusão do usuário", description = "Essa operação excluí o usuário por código.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Usuário excluído com sucesso.") })
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity edit(
			@Parameter(name = "id", description = "Codígo do usuário.", example = "123", required = true) @PathVariable BigInteger id) {

		try {
			this.usuarioService.deletar(id);
			return new ResponseEntity(HttpStatus.OK);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
