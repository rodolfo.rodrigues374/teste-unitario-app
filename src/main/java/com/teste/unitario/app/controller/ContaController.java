package com.teste.unitario.app.controller;

import java.math.BigInteger;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.unitario.app.dto.request.conta.ContaCadastrarRequest;
import com.teste.unitario.app.dto.request.conta.ContaEditarRequest;
import com.teste.unitario.app.dto.response.conta.ContaResponse;
import com.teste.unitario.app.service.interfaces.IContaService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/conta")
@SuppressWarnings({ "unchecked", "rawtypes" })
@Tag(name = "conta", description = "Conta API")
public class ContaController {

	@Autowired
	private IContaService contaService;

	@Operation(summary = "Retorna uma lista de contas", description = "Essa operação retorna as informações de contas.", tags = {
			"conta" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Lista de contas retornada com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContaResponse.class)))) })
	@GetMapping(value = "/get")
	public ResponseEntity get() {

		try {
			var lista = this.contaService.listar();
			return new ResponseEntity(lista, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Salva o conta", description = "Essa operação salva as informações de contas.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "conta salvo com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContaCadastrarRequest.class)))) })
	@PostMapping(value = "/save")
	public ResponseEntity save(@RequestBody @Valid ContaCadastrarRequest request) {

		try {
			this.contaService.cadastrar(request);
			return new ResponseEntity(HttpStatus.CREATED);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Pesquisa o conta", description = "Essa operação pesquisa as informações do conta por código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "conta pesquisado com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContaResponse.class)))) })
	@GetMapping(value = "/get/{id}")
	public ResponseEntity getId(
			@Parameter(name = "id", description = "Codígo do conta.", example = "123", required = true) @PathVariable BigInteger id) {

		try {

			return new ResponseEntity(this.contaService.obter(id), HttpStatus.OK);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Alteração do conta", description = "Essa operação altera as informações do conta por código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "conta alerado com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ContaEditarRequest.class)))) })
	@PutMapping(value = "/edit")
	public ResponseEntity edit(@RequestBody @Valid ContaEditarRequest request) {

		try {
			this.contaService.editar(request);
			return new ResponseEntity(HttpStatus.OK);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Exclusão do conta", description = "Essa operação excluí o conta por código.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Conta excluído com sucesso.") })
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity edit(
			@Parameter(name = "id", description = "Codígo do conta.", example = "123", required = true) @PathVariable BigInteger id) {

		try {
			this.contaService.deletar(id);
			return new ResponseEntity(HttpStatus.OK);
		} catch (ValidationException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
