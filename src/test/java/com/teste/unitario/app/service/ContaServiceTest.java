package com.teste.unitario.app.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.ValidationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.teste.unitario.app.builder.conta.ContaCadastrarRequestBuilder;
import com.teste.unitario.app.builder.conta.ContaEditarRequestBuilder;
import com.teste.unitario.app.builder.conta.ContaEntityBuilder;
import com.teste.unitario.app.builder.conta.ContaResponseBuilder;
import com.teste.unitario.app.builder.usuario.UsuarioEntityBuilder;
import com.teste.unitario.app.dto.response.conta.ContaResponse;
import com.teste.unitario.app.entity.ContaEntity;
import com.teste.unitario.app.mapper.ContaMapper;
import com.teste.unitario.app.repository.ContaRepository;
import com.teste.unitario.app.repository.UsuarioRepository;
import com.teste.unitario.app.service.impl.ContaService;
import com.teste.unitario.app.utils.Mensagem;

public class ContaServiceTest {

	@InjectMocks
	private ContaService service;

	@Mock
	private ContaMapper contaMapper;

	@Mock
	private ContaRepository contaRepository;

	@Mock
	private UsuarioRepository usuarioRepository;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	private static final String NUM_CONTA = "123456";

	@Test
	public void listar() {

		// cenario
		List<ContaEntity> list = Arrays.asList(ContaEntityBuilder.umaConta().agora(),
				ContaEntityBuilder.umaConta().agora());

		List<ContaResponse> listResponse = Arrays.asList(ContaResponseBuilder.umaConta().agora(),
				ContaResponseBuilder.umaConta().agora());

		Mockito.when(contaRepository.findAll()).thenReturn(list);
		Mockito.when(contaMapper.mapper(list)).thenReturn(listResponse);

		// acao
		var retorno = service.listar();

		Assert.assertEquals(retorno.size(), 2);

	}

	@Test
	public void cadatrarNumAgenciaNulo() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().setNumAgencia(null).agora();

		try {
			// Acao
			service.cadastrar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void cadatrarNumContaNulo() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().setNumConta(null).agora();

		try {
			// Acao
			service.cadastrar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void cadatrarIdUsuarioNulo() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().setIdUsuario(null).agora();

		try {
			// Acao
			service.cadastrar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void cadatrarTipoContaNulo() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().setTipoConta(null).agora();

		try {
			// Acao
			service.cadastrar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void cadatrarNumAgenciaJaCadastrado() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().setNumAgencia(NUM_CONTA).agora();

		var contacadastrada = ContaEntityBuilder.umaConta().setNumAgencia(NUM_CONTA).agoraOptional();

		var contaEntity = ContaEntityBuilder.umaConta().setNumAgencia(NUM_CONTA).agora();

		Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity);
		Mockito.when(contaRepository.findByNumAgencia(contaEntity.getNumAgencia())).thenReturn(contacadastrada);
		exception.expectMessage(Mensagem.NUM_AGENCIA_CADASTRADO);

		// Acao
		service.cadastrar(request);
	}

	@Test
	public void cadatrarNumContaJaCadastrado() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().setNumConta(NUM_CONTA).agora();

		var contaCadastrada = ContaEntityBuilder.umaConta().setNumConta(NUM_CONTA).agoraOptional();

		var contaEntity = ContaEntityBuilder.umaConta().setNumConta(NUM_CONTA).agora();

		Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity);
		Mockito.when(contaRepository.findByNumConta(contaEntity.getNumConta())).thenReturn(contaCadastrada);
		exception.expectMessage(Mensagem.NUM_CONTA_CADASTRADO);

		// Acao
		service.cadastrar(request);
	}

	@Test
	public void verificaUsuarioEhMaior18AnosIdUsuarioNulo() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().agora();

		var usuarioCadastrada = UsuarioEntityBuilder.umUsuario().agoraOptional();

		var contaEntity = ContaEntityBuilder.umaConta().agora();

		Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity);

		Mockito.when(usuarioRepository.findById(null)).thenReturn(usuarioCadastrada);
		exception.expectMessage(Mensagem.USUARIO_NAO_ENCONTRADO);

		// Acao
		service.cadastrar(request);
	}

	@Test
	public void verificaUsuarioEhMaior18AnosIdUsuarioNaoEncontrado() {

		// Cenario
		var request = ContaCadastrarRequestBuilder.umaConta().agora();

		var usuarioCadastrada = UsuarioEntityBuilder.umUsuario().agoraOptional();

		var contaEntity = ContaEntityBuilder.umaConta().agora();

		Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity);

		Mockito.when(usuarioRepository.findById(BigInteger.TWO)).thenReturn(usuarioCadastrada);
		exception.expectMessage(Mensagem.USUARIO_NAO_ENCONTRADO);

		// Acao
		service.cadastrar(request);
	}

	@Test
	public void verificaUsuarioEhMenor18Anos()  {

		LocalDateTime dataAtual = LocalDateTime.of(2021, 4, 12, 0, 0, 0);

		var request = ContaCadastrarRequestBuilder.umaConta().agora();
		var usuarioCadastrada = UsuarioEntityBuilder.umUsuario()
				.setDataNascimento(LocalDateTime.of(2020, 4, 12, 0, 0, 0)).agoraOptional();

		var contaEntity = ContaEntityBuilder.umaConta().agora();

		try (MockedStatic<LocalDateTime> mock = Mockito.mockStatic(LocalDateTime.class)) {

			mock.when(() -> LocalDateTime.now()).thenReturn(dataAtual);
			
			// mock
			Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity);
			Mockito.when(usuarioRepository.findById(contaEntity.getUsuario().getIdUsuario()))
					.thenReturn(usuarioCadastrada);

			// acao
			exception.expectMessage(Mensagem.USUARIO_IDADE_MENOR_18_ANOS);
			service.cadastrar(request);
		}

	}

	@Test
	public void verificaUsuarioEhMaior18Anos()   {

		LocalDateTime dataAtual = LocalDateTime.of(2021, 4, 12, 0, 0, 0);

		var request = ContaCadastrarRequestBuilder.umaConta().agora();
		var usuarioCadastrada = UsuarioEntityBuilder.umUsuario()
				.setDataNascimento(LocalDateTime.of(1989, 4, 12, 0, 0, 0)).agoraOptional();

		var contaEntity = ContaEntityBuilder.umaConta().agora();

		try (MockedStatic<LocalDateTime> mock = Mockito.mockStatic(LocalDateTime.class)) {

			mock.when(() -> LocalDateTime.now()).thenReturn(dataAtual);

			// mock
			Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity);
			Mockito.when(usuarioRepository.findById(contaEntity.getUsuario().getIdUsuario()))
					.thenReturn(usuarioCadastrada);

			// acao
			try {
				service.cadastrar(request);
				Assert.assertTrue(true);
			} catch (Exception e) {
				Assert.fail();
			}
		}

	}

	@Test
	public void editarIdNulo() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().setId(null).agora();

		try {
			// Acao
			service.editar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CODIGO_OBRIGATORIO);
		}
	}

	@Test
	public void editarIdUsuarioNulo() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().setIdUsuario(null).agora();

		try {
			// Acao
			service.editar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void editarNumAgenciaNulo() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().setNumAgencia(null).agora();

		try {
			// Acao
			service.editar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void editarNumContaNulo() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().setNumConta(null).agora();

		try {
			// Acao
			service.editar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void editarTipoContaNulo() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().setTipoConta(null).agora();

		try {
			// Acao
			service.editar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}
	}

	@Test
	public void editarContaRegistroNaoEncontrado() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().agora();

		// Mock
		Mockito.when(usuarioRepository.findById(request.getId())).thenReturn(Optional.empty());

		// Acao
		exception.expectMessage(Mensagem.REGISTRO_NAO_ENCONTRADO);
		service.editar(request);

	}

	@Test
	public void editarComSucesso() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().agora();

		var contaEntity = ContaEntityBuilder.umaConta().agoraOptional();

		// Mock
		Mockito.when(contaRepository.findById(request.getId())).thenReturn(contaEntity);

		Mockito.when(contaMapper.mapper(request)).thenReturn(contaEntity.get());

		// Acao
		try {
			service.editar(request);
			Assert.assertTrue(true);
		} catch (ValidationException e) {
			Assert.fail();
		}
	}

	@Test
	public void obterContaRegistroNaoEncontrado() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().agora();
		var contaEntity = ContaEntityBuilder.umaConta().agoraOptional(); 
		
		// Mock
		Mockito.when(contaRepository.findById(BigInteger.TWO)).thenReturn(contaEntity);
	 
		// Acao
		exception.expectMessage(Mensagem.REGISTRO_NAO_ENCONTRADO);
		service.obter(request.getId());

	}

	@Test
	public void obterContaComSucesso() {

		// Cenario
		var request = ContaEditarRequestBuilder.umaConta().agora();

		var contaCadastrado = ContaEntityBuilder.umaConta().agoraOptional();

		// Mock
		Mockito.when(contaRepository.findById(request.getId())).thenReturn(contaCadastrado);

		var response = new ContaResponse();
		response.setIdConta(contaCadastrado.get().getIdConta());
		response.setIdUsuario(contaCadastrado.get().getUsuario().getIdUsuario());
		response.setNumAgencia(contaCadastrado.get().getNumAgencia());
		response.setNumConta(contaCadastrado.get().getNumConta());
		response.setTipoConta(contaCadastrado.get().getTipoConta());
		response.setDataCriacao(contaCadastrado.get().getDataCriacao());
		response.setDataAlteracao(contaCadastrado.get().getDataAlteracao());

		Mockito.when(contaMapper.mapper(contaCadastrado.get())).thenReturn(response);

		// Acao
		var retorno = service.obter(request.getId());
		Assert.assertNotNull(retorno.getIdConta());

	}

	@Test
	public void deletarContaCodigoNulo() {

		// Cenario
		BigInteger id = null;

		// Acao
		exception.expectMessage(Mensagem.CODIGO_OBRIGATORIO);

		// Acao
		service.deletar(id);
	}

	@Test
	public void deletarContaRegistroNaoEncontrado() {

		// Cenario
		BigInteger id = BigInteger.valueOf(2L);

		// Mock
		Mockito.when(contaRepository.findById(id)).thenReturn(Optional.empty());

		exception.expectMessage(Mensagem.REGISTRO_NAO_ENCONTRADO);

		// Acao
		service.deletar(id);
	}

	@Test
	public void deletarContaRegistroComSucesso() {

		// Cenario
		BigInteger id = BigInteger.valueOf(1L);

		// Mock
		var contaCadastrado = ContaEntityBuilder.umaConta().agoraOptional();
		Mockito.when(contaRepository.findById(id)).thenReturn(contaCadastrado);

		// Acao

		try {
			service.deletar(id);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail();
		}

	}

}
