package com.teste.unitario.app.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.ValidationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.teste.unitario.app.builder.usuario.UsuarioCadastrarRequestBuilder;
import com.teste.unitario.app.builder.usuario.UsuarioEditarRequestBuilder;
import com.teste.unitario.app.builder.usuario.UsuarioEntityBuilder;
import com.teste.unitario.app.builder.usuario.UsuarioResponseBuilder;
import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;
import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;
import com.teste.unitario.app.entity.UsuarioEntity;
import com.teste.unitario.app.mapper.UsuarioMapper;
import com.teste.unitario.app.repository.UsuarioRepository;
import com.teste.unitario.app.service.impl.UsuarioService;
import com.teste.unitario.app.utils.Mensagem;

public class UsuarioServiceTest {

	@InjectMocks
	private UsuarioService service;

	@Mock
	private UsuarioMapper usuarioMapper;

	@Mock
	private UsuarioRepository usuarioRepository;

	private UsuarioEntity userEntity = new UsuarioEntity();

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void listar() {

		// cenario
		List<UsuarioEntity> list = Arrays.asList(UsuarioEntityBuilder.umUsuario().agora(),
				UsuarioEntityBuilder.umUsuario().agora());

		List<UsuarioResponse> listResponse = Arrays.asList(UsuarioResponseBuilder.umUsuario().agora(),
				UsuarioResponseBuilder.umUsuario().agora());

		Mockito.when(usuarioRepository.findAll()).thenReturn(list);
		Mockito.when(usuarioMapper.mapper(list)).thenReturn(listResponse);

		// acao
		var retorno = service.listar();

		Assert.assertEquals(retorno.size(), 2);

	}

	@Test
	public void cadatrarUsuarioEmailNomeNulo() {

		// Cenario
		var request = new UsuarioCadastrarRequest();

		try {

			// Acao
			service.cadastrar(request);
			Assert.fail();
		} catch (ValidationException e) {
			Assert.assertEquals(e.getMessage(), Mensagem.CAMPO_OBRIGATORIO);
		}

	}

	@Test
	public void cadatrarUsuarioEmailExistente() {

		// Cenario
		var request = UsuarioCadastrarRequestBuilder.umUsuario().agora();

		// Mock
		Optional<UsuarioEntity> usuarioCadastrado = UsuarioEntityBuilder.umUsuario().agoraOptional();
		userEntity = UsuarioEntityBuilder.umUsuario().agora();

		Mockito.when(usuarioMapper.mapper(request)).thenReturn(userEntity);
		Mockito.when(usuarioRepository.findByEmail(userEntity.getEmail())).thenReturn(usuarioCadastrado);

		exception.expectMessage(Mensagem.EMAIL_CADASTRADO);

		// Acao
		service.cadastrar(request);

	}

	@Test
	public void cadatrarUsuarioEmailInvalido() {

		// Cenario
		var request = UsuarioCadastrarRequestBuilder.umUsuario().setEmail("dsadsadsd").agora();

		// Mock
		userEntity = UsuarioEntityBuilder.umUsuario().setEmail("dsadsadsd").agora();

		Mockito.when(usuarioMapper.mapper(request)).thenReturn(userEntity);
		Mockito.when(usuarioRepository.findByEmail(userEntity.getEmail())).thenReturn(Optional.empty());

		exception.expectMessage(Mensagem.EMAIL_INVALIDO);

		// Acao
		service.cadastrar(request);

	}

	@Test
	public void cadatrarUsuarioComSucesso() {

		// Cenario
		var request = UsuarioCadastrarRequestBuilder.umUsuario().agora();

		// Mock
		userEntity = UsuarioEntityBuilder.umUsuario().agora();
		Mockito.when(usuarioMapper.mapper(request)).thenReturn(userEntity);
		Mockito.when(usuarioRepository.findByEmail(userEntity.getEmail())).thenReturn(Optional.empty());

		// Acao

		try {
			service.cadastrar(request);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void editarUsuarioCodigoNulo() {

		// Cenario
		var request = UsuarioEditarRequestBuilder.umUsuario().agora();
		request.setId(null);

		// Acao
		exception.expectMessage(Mensagem.CODIGO_OBRIGATORIO);

		// Acao
		service.editar(request);
	}

	@Test
	public void editarUsuarioRegistroNaoEncontrado() {

		// Cenario
		var request = UsuarioEditarRequestBuilder.umUsuario().agora();

		// Mock
		Mockito.when(usuarioRepository.findById(request.getId())).thenReturn(Optional.empty());

		exception.expectMessage(Mensagem.REGISTRO_NAO_ENCONTRADO);

		// Acao
		service.editar(request);
	}

	@Test
	public void editarUsuarioComSucesso() {

		// Cenario
		var request = UsuarioEditarRequestBuilder.umUsuario().agora();

		// Mock
		Optional<UsuarioEntity> usuarioCadastrado = UsuarioEntityBuilder.umUsuario().agoraOptional();
		Mockito.when(usuarioRepository.findById(request.getId())).thenReturn(usuarioCadastrado);

		if (usuarioCadastrado.isPresent()) {
			var entity = usuarioCadastrado.get();
			entity.setNome(request.getNome());
			entity.setEmail(request.getEmail());
			entity.setDataNascimento(request.getDataNascimento());
			entity.setDataAlteracao(LocalDateTime.now());
		}

		// Acao
		try {
			service.editar(request);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void obterUsuarioCodigoNulo() {

		// Cenario
		BigInteger id = null;

		// Acao
		exception.expectMessage(Mensagem.CODIGO_OBRIGATORIO);

		// Acao
		service.obter(id);
	}

	@Test
	public void obterUsuarioRegistroNaoEncontrado() {

		// Cenario
		BigInteger id = BigInteger.valueOf(2L);

		// Mock
		Mockito.when(usuarioRepository.findById(id)).thenReturn(Optional.empty());

		exception.expectMessage(Mensagem.REGISTRO_NAO_ENCONTRADO);

		// Acao
		service.obter(id);
	}

	@Test
	public void obterUsuarioRegistroComSucesso() {

		// Cenario
		BigInteger id = BigInteger.valueOf(1L);

		// Mock
		Optional<UsuarioEntity> usuarioCadastrado = UsuarioEntityBuilder.umUsuario().agoraOptional();
		Mockito.when(usuarioRepository.findById(id)).thenReturn(usuarioCadastrado);

		if (usuarioCadastrado.isPresent()) {
			var response = new UsuarioResponse();
			response.setId(usuarioCadastrado.get().getIdUsuario());
			response.setNome(usuarioCadastrado.get().getNome());
			response.setEmail(usuarioCadastrado.get().getEmail());
			response.setDataNascimento(usuarioCadastrado.get().getDataNascimento());
			response.setDataCriacao(usuarioCadastrado.get().getDataCriacao());
			response.setDataAlteracao(usuarioCadastrado.get().getDataAlteracao());

			Mockito.when(usuarioMapper.mapper(usuarioCadastrado.get())).thenReturn(response);
		}
		// Acao
		var retorno = service.obter(id);

		Assert.assertNotNull(retorno.getId());
	}

	@Test
	public void deletarUsuarioCodigoNulo() {

		// Cenario
		BigInteger id = null;

		// Acao
		exception.expectMessage(Mensagem.CODIGO_OBRIGATORIO);

		// Acao
		service.deletar(id);
	}

	@Test
	public void deletarUsuarioRegistroNaoEncontrado() {

		// Cenario
		BigInteger id = BigInteger.valueOf(2L);

		// Mock
		Mockito.when(usuarioRepository.findById(id)).thenReturn(Optional.empty());

		exception.expectMessage(Mensagem.REGISTRO_NAO_ENCONTRADO);

		// Acao
		service.deletar(id);
	}

	@Test
	public void deletarUsuarioRegistroComSucesso() {

		// Cenario
		BigInteger id = BigInteger.valueOf(1L);

		// Mock
		Optional<UsuarioEntity> usuarioCadastrado = UsuarioEntityBuilder.umUsuario().agoraOptional();
		Mockito.when(usuarioRepository.findById(id)).thenReturn(usuarioCadastrado);

		// Acao

		try {
			service.deletar(id);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail();
		}

	}
}
