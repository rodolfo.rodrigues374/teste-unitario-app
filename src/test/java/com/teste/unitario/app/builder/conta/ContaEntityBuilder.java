package com.teste.unitario.app.builder.conta; 
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Optional;

import com.teste.unitario.app.builder.usuario.UsuarioEntityBuilder;
import com.teste.unitario.app.entity.ContaEntity;
import com.teste.unitario.app.enuns.TipoContaEnum; 

public class ContaEntityBuilder {

	private ContaEntity elemento;
	private ContaEntityBuilder(){}
	
	public static ContaEntityBuilder umaConta() {
		ContaEntityBuilder builder = new ContaEntityBuilder();
		inicializarDadosPadroes(builder);
		return builder;
	}
	
	public static void inicializarDadosPadroes(ContaEntityBuilder builder) {
		builder.elemento = new ContaEntity();
		ContaEntity elemento = builder.elemento;

		elemento.setIdConta(BigInteger.valueOf(1L));
		elemento.setNumConta("123456");
		elemento.setNumAgencia("123");
		elemento.setUsuario(UsuarioEntityBuilder.umUsuario().agora());
		elemento.setTipoConta(TipoContaEnum.CORRENTE);
		elemento.setDataCriacao(LocalDateTime.now());
		elemento.setDataAlteracao(LocalDateTime.now()); 
	}
	 
	
	public ContaEntity agora() {
		return elemento;
	}
	
	public ContaEntityBuilder setNumConta(String valor) {
		elemento.setNumConta(valor);
		return this;
	} 
	
	public ContaEntityBuilder setNumAgencia(String valor) {
		elemento.setNumAgencia(valor);
		return this;
	} 
	
	public ContaEntityBuilder setUsuarioDataNascimento(LocalDateTime valor) {
		elemento.getUsuario().setDataNascimento(valor);
		return this;
	} 
	
	public Optional<ContaEntity> agoraOptional() {
		return Optional.of(elemento);
	}
}
