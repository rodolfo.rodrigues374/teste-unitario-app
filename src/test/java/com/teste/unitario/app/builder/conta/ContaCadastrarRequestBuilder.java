package com.teste.unitario.app.builder.conta;

import java.math.BigInteger;

import com.teste.unitario.app.dto.request.conta.ContaCadastrarRequest;
import com.teste.unitario.app.enuns.TipoContaEnum;

public class ContaCadastrarRequestBuilder {

	private ContaCadastrarRequest request;

	private ContaCadastrarRequestBuilder() {
	}

	public static ContaCadastrarRequestBuilder umaConta() {
		ContaCadastrarRequestBuilder builder = new ContaCadastrarRequestBuilder();
		builder.request = new ContaCadastrarRequest();
		builder.request.setNumConta("123456");
		builder.request.setNumAgencia("123");	 
		builder.request.setIdUsuario(BigInteger.ONE);
		builder.request.setTipoConta(TipoContaEnum.CORRENTE);
		return builder;
	}

	public ContaCadastrarRequestBuilder comNumConta(String valor) {
		request.setNumConta(valor);
		return this;
	}
	
	public ContaCadastrarRequestBuilder setNumConta(String valor) {
		request.setNumConta(valor);
		return this;
	} 
	
	public ContaCadastrarRequestBuilder setNumAgencia(String valor) {
		request.setNumAgencia(valor);
		return this;
	} 
	
	public ContaCadastrarRequestBuilder setIdUsuario(BigInteger valor) {
		request.setIdUsuario(valor);
		return this;
	} 
	
	public ContaCadastrarRequestBuilder setTipoConta(TipoContaEnum valor) {
		request.setTipoConta(valor);
		return this;
	} 

	public ContaCadastrarRequest agora() {
		return request;
	}
}
