package com.teste.unitario.app.builder.usuario;

import java.math.BigInteger;
import java.time.LocalDateTime;

import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;

public class UsuarioResponseBuilder {

	private UsuarioResponse usuario;

	private UsuarioResponseBuilder() {
	}

	public static UsuarioResponseBuilder umUsuario() {
		UsuarioResponseBuilder builder = new UsuarioResponseBuilder();
		builder.usuario = new UsuarioResponse();
		builder.usuario.setId(BigInteger.ONE);
		builder.usuario.setNome("Usuario 1");
		builder.usuario.setEmail("rodolfo@email.com");
		builder.usuario.setDataNascimento(LocalDateTime.now());
		return builder;
	}

	public UsuarioResponseBuilder comNome(String nome) {
		usuario.setNome(nome);
		return this;
	}
	
	public UsuarioResponseBuilder setEmail(String email) {
		usuario.setEmail(email);
		return this;
	} 

	public UsuarioResponse agora() {
		return usuario;
	}
}
