package com.teste.unitario.app.builder.usuario;

import java.math.BigInteger;
import java.time.LocalDateTime;

import com.teste.unitario.app.dto.request.usuario.UsuarioEditarRequest;

public class UsuarioEditarRequestBuilder {

	private UsuarioEditarRequest usuario;

	private UsuarioEditarRequestBuilder() {
	}

	public static UsuarioEditarRequestBuilder umUsuario() {
		UsuarioEditarRequestBuilder builder = new UsuarioEditarRequestBuilder();
		builder.usuario = new UsuarioEditarRequest();
		builder.usuario.setId(BigInteger.ONE);
		builder.usuario.setNome("Usuario 1");
		builder.usuario.setEmail("rodolfo@email.com");
		builder.usuario.setDataNascimento(LocalDateTime.now());
		return builder;
	}

	public UsuarioEditarRequestBuilder comNome(String valor) {
		usuario.setNome(valor);
		return this;
	}
	
	public UsuarioEditarRequestBuilder setEmail(String valor) {
		usuario.setEmail(valor);
		return this;
	} 
	
	public UsuarioEditarRequestBuilder setId(BigInteger valor) {
		usuario.setId(valor);
		return this;
	} 

	public UsuarioEditarRequest agora() {
		return usuario;
	}
}
