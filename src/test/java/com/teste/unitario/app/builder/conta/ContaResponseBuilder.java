package com.teste.unitario.app.builder.conta;

import java.math.BigInteger;

import com.teste.unitario.app.dto.response.conta.ContaResponse;
import com.teste.unitario.app.enuns.TipoContaEnum;

public class ContaResponseBuilder {

	private ContaResponse response;

	private ContaResponseBuilder() {
	}

	public static ContaResponseBuilder umaConta() {
		ContaResponseBuilder builder = new ContaResponseBuilder();
		builder.response = new ContaResponse();
		builder.response.setIdConta(BigInteger.ONE);
		builder.response.setNumConta("123456");
		builder.response.setNumAgencia("123");
		builder.response.setIdUsuario(BigInteger.ONE);
		builder.response.setTipoConta(TipoContaEnum.CORRENTE);
		return builder;
	}

 

	public ContaResponse agora() {
		return response;
	}
}
