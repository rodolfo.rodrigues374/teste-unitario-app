package com.teste.unitario.app.builder.conta;

import java.math.BigInteger;

import com.teste.unitario.app.dto.request.conta.ContaEditarRequest;
import com.teste.unitario.app.enuns.TipoContaEnum;

public class ContaEditarRequestBuilder {

	private ContaEditarRequest request;

	private ContaEditarRequestBuilder() {
	}

	public static ContaEditarRequestBuilder umaConta() {
		ContaEditarRequestBuilder builder = new ContaEditarRequestBuilder();
		builder.request = new ContaEditarRequest();
		builder.request.setId(BigInteger.ONE);
		builder.request.setNumConta("123456");
		builder.request.setNumAgencia("123");
		builder.request.setIdUsuario(BigInteger.ONE);
		builder.request.setTipoConta(TipoContaEnum.CORRENTE);
		return builder;
	}

	public ContaEditarRequestBuilder comNumConta(String valor) {
		request.setNumConta(valor);
		return this;
	}
	
	public ContaEditarRequestBuilder setNumConta(String valor) {
		request.setNumConta(valor);
		return this;
	} 
	
	public ContaEditarRequestBuilder setNumAgencia(String valor) {
		request.setNumAgencia(valor);
		return this;
	} 
	
	public ContaEditarRequestBuilder setIdUsuario(BigInteger valor) {
		request.setIdUsuario(valor);
		return this;
	} 
	
	public ContaEditarRequestBuilder setTipoConta(TipoContaEnum valor) {
		request.setTipoConta(valor);
		return this;
	} 
	
	public ContaEditarRequestBuilder setId(BigInteger valor) {
		request.setId(valor);
		return this;
	} 

	public ContaEditarRequest agora() {
		return request;
	}
}
