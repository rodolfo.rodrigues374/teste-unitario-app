package com.teste.unitario.app.builder.usuario; 
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Optional;

import com.teste.unitario.app.entity.UsuarioEntity; 

public class UsuarioEntityBuilder {

	private UsuarioEntity elemento;
	private UsuarioEntityBuilder(){}
	
	public static UsuarioEntityBuilder umUsuario() {
		UsuarioEntityBuilder builder = new UsuarioEntityBuilder();
		inicializarDadosPadroes(builder);
		return builder;
	}
	
	public static void inicializarDadosPadroes(UsuarioEntityBuilder builder) {
		builder.elemento = new UsuarioEntity();
		UsuarioEntity elemento = builder.elemento;

		elemento.setIdUsuario(BigInteger.valueOf(1L));
		elemento.setNome("Rodolfo");
		elemento.setEmail("Rodolfo@com.br");
		elemento.setDataNascimento(LocalDateTime.now());
		elemento.setDataCriacao(LocalDateTime.now());
		elemento.setDataAlteracao(LocalDateTime.now()); 
	}
	
	public UsuarioEntityBuilder setEmail(String valor) {
		elemento.setEmail(valor);
		return this;
	}
	
	public UsuarioEntityBuilder setDataNascimento(LocalDateTime valor) {
		elemento.setDataNascimento(valor);
		return this;
	}
	
	public UsuarioEntity agora() {
		return elemento;
	}
	
	public Optional<UsuarioEntity> agoraOptional() {
		return Optional.of(elemento);
	}
}
