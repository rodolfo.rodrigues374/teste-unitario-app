package com.teste.unitario.app.builder.usuario;

import java.time.LocalDateTime;

import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;

public class UsuarioCadastrarRequestBuilder {

	private UsuarioCadastrarRequest usuario;

	private UsuarioCadastrarRequestBuilder() {
	}

	public static UsuarioCadastrarRequestBuilder umUsuario() {
		UsuarioCadastrarRequestBuilder builder = new UsuarioCadastrarRequestBuilder();
		builder.usuario = new UsuarioCadastrarRequest();
		builder.usuario.setNome("Usuario 1");
		builder.usuario.setEmail("rodolfo@email.com");
		builder.usuario.setDataNascimento(LocalDateTime.now());
		return builder;
	}

	public UsuarioCadastrarRequestBuilder comNome(String nome) {
		usuario.setNome(nome);
		return this;
	}
	
	public UsuarioCadastrarRequestBuilder setEmail(String email) {
		usuario.setEmail(email);
		return this;
	} 

	public UsuarioCadastrarRequest agora() {
		return usuario;
	}
}
