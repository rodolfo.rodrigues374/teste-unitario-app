package com.teste.unitario.app.controller;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.teste.unitario.app.builder.conta.ContaCadastrarRequestBuilder;
import com.teste.unitario.app.builder.conta.ContaEditarRequestBuilder;
import com.teste.unitario.app.dto.request.conta.ContaCadastrarRequest;
import com.teste.unitario.app.dto.request.conta.ContaEditarRequest;
import com.teste.unitario.app.dto.response.conta.ContaResponse;
import com.teste.unitario.app.utils.Base;
import com.teste.unitario.app.utils.Mensagem;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContaControllerTest extends Base {

	
	
	
	@Test
	public void get()   {
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("conta/get"), HttpMethod.GET, entity,
				String.class);

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
			@Override
			public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {

				return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			}

		}).create();

		ContaResponse[] contaList = gson.fromJson(response.getBody(), ContaResponse[].class);
		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assert.assertTrue(contaList.length > 0);

	}

	@Test
	public void saveSucesso() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		var conta = ContaCadastrarRequestBuilder.umaConta().agora();
		HttpEntity<ContaCadastrarRequest> requestEntity = new HttpEntity<>(conta, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/save"), HttpMethod.POST,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
	}

	@Test
	public void saveCampoNulos() throws Exception {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<ContaCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/save"), HttpMethod.POST,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void getbyidNaoEncontrado() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<ContaCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/get/34"), HttpMethod.GET,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		Assert.assertEquals(response.getBody(), Mensagem.REGISTRO_NAO_ENCONTRADO);
	}

	@Test
	public void getbyidComSucesso() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<ContaCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/get/" + BigInteger.ONE),
				HttpMethod.GET, requestEntity, String.class);

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
			@Override
			public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {

				return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			}

		}).create();

		ContaResponse contaResponse = gson.fromJson(response.getBody(), ContaResponse.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assert.assertEquals(contaResponse.getIdConta(), BigInteger.ONE);
	}
	
	
	@Test
	public void deletebyIdNaoEncontrado()  {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<ContaCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/delete/3"), HttpMethod.DELETE,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		Assert.assertEquals(response.getBody(), Mensagem.REGISTRO_NAO_ENCONTRADO);
	}
	
	@Test
	public void deletebyIdComSucesso()  {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<ContaCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/delete/2"), HttpMethod.DELETE,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void editarIdNaoEncontrado(){

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		var conta = ContaEditarRequestBuilder.umaConta().setId(BigInteger.ZERO).agora();
		HttpEntity<ContaEditarRequest> requestEntity = new HttpEntity<>(conta, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/edit"), HttpMethod.PUT,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		Assert.assertEquals(response.getBody(), Mensagem.REGISTRO_NAO_ENCONTRADO);
	}
	
	@Test
	public void editarSucesso() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		var conta = ContaEditarRequestBuilder.umaConta().setNumAgencia("4564").agora();
		HttpEntity<ContaEditarRequest> requestEntity = new HttpEntity<>(conta, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/conta/edit"), HttpMethod.PUT,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
	}

}
