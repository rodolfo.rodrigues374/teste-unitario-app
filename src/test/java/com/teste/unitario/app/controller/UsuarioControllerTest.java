package com.teste.unitario.app.controller;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.teste.unitario.app.builder.usuario.UsuarioCadastrarRequestBuilder;
import com.teste.unitario.app.builder.usuario.UsuarioEditarRequestBuilder;
import com.teste.unitario.app.dto.request.usuario.UsuarioCadastrarRequest;
import com.teste.unitario.app.dto.request.usuario.UsuarioEditarRequest;
import com.teste.unitario.app.dto.response.usuario.UsuarioResponse;
import com.teste.unitario.app.utils.Base;
import com.teste.unitario.app.utils.Mensagem;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsuarioControllerTest extends Base {

	@Test
	public void get()   {
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/get"), HttpMethod.GET, entity,
				String.class);

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
			@Override
			public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {

				return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			}

		}).create();

		UsuarioResponse[] usuarioList = gson.fromJson(response.getBody(), UsuarioResponse[].class);
		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assert.assertTrue(usuarioList.length > 0);

	}

	@Test
	public void saveSucesso() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		var usuario = UsuarioCadastrarRequestBuilder.umUsuario().setEmail("rodolfo@mail.com").agora();
		HttpEntity<UsuarioCadastrarRequest> requestEntity = new HttpEntity<>(usuario, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/save"), HttpMethod.POST,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
	}

	@Test
	public void saveCampoNulos() throws Exception {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<UsuarioCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/save"), HttpMethod.POST,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void getbyidNaoEncontrado() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<UsuarioCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/get/34"), HttpMethod.GET,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		Assert.assertEquals(response.getBody(), Mensagem.REGISTRO_NAO_ENCONTRADO);
	}

	@Test
	public void getbyidComSucesso() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<UsuarioCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/get/" + BigInteger.ONE),
				HttpMethod.GET, requestEntity, String.class);

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
			@Override
			public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {

				return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			}

		}).create();

		UsuarioResponse usuarioResponse = gson.fromJson(response.getBody(), UsuarioResponse.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assert.assertEquals(usuarioResponse.getId(), BigInteger.ONE);
	}
	
	
	@Test
	public void deletebyIdNaoEncontrado()  {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<UsuarioCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/delete/3"), HttpMethod.DELETE,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		Assert.assertEquals(response.getBody(), Mensagem.REGISTRO_NAO_ENCONTRADO);
	}
	
	@Test
	public void deletebyIdComSucesso()  {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<UsuarioCadastrarRequest> requestEntity = new HttpEntity<>(null, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/delete/2"), HttpMethod.DELETE,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void editarIdNaoEncontrado(){

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		var usuario = UsuarioEditarRequestBuilder.umUsuario().setId(BigInteger.ZERO).setEmail("rodolfo.editado@mail.com").agora();
		HttpEntity<UsuarioEditarRequest> requestEntity = new HttpEntity<>(usuario, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/edit"), HttpMethod.PUT,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		Assert.assertEquals(response.getBody(), Mensagem.REGISTRO_NAO_ENCONTRADO);
	}
	
	@Test
	public void editarSucesso() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		var usuario = UsuarioEditarRequestBuilder.umUsuario().setEmail("rodolfo.editado@mail.com").agora();
		HttpEntity<UsuarioEditarRequest> requestEntity = new HttpEntity<>(usuario, requestHeaders);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/usuario/edit"), HttpMethod.PUT,
				requestEntity, String.class);

		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
	}

}
